package com.example.jwtbasic.JwtUserDetailsService;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User; 
import org.springframework.security.core.userdetails.UserDetails; 
import org.springframework.security.core.userdetails.UserDetailsService; 
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.jwtbasic.dao.UserDao;
import com.example.jwtbasic.model.DAOUser;
import com.example.jwtbasic.model.UserDTO;

//import com.javainuse.dao.UserDao; 
@Service
public class JwtUserDetailsService implements UserDetailsService { 
	
	@Autowired
	private UserDao userDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;
	
   @Override 
   public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//      if ("randomuser123".equals(username)) { 
//         return new User("randomuser123", 
//            "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6", 
//            new ArrayList<>()); 
//      } else { 
//         throw new UsernameNotFoundException("User not found with username: " + username); 
//      } 
	   DAOUser user =  userDao.findByUsername(username);
       if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                new ArrayList<>());
   } 
   
   public ResponseEntity<?> savecheck(UserDTO user) {
	   
	   Iterable<DAOUser> check = get();
	   int count = 0;
	   for(DAOUser checked : check) {
		   
		   if(checked.getUsername().equalsIgnoreCase(user.getUsername())) {
			   
			count++;
		   }
	   }
	   if(count==0) {
		return ResponseEntity.ok(save(user));
	   }
	   else {
		return ResponseEntity.ok("user already exist");
	   }
   }
   
   
   public DAOUser save(UserDTO user) {
	   
		DAOUser newUser = new DAOUser();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		return userDao.save(newUser);
	}
   
public Iterable<DAOUser> get() {

	return userDao.findAll();
}

}