package com.example.jwtbasic.JwtController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.jwtbasic.JwtRequestModel.JwtRequestModel;
import com.example.jwtbasic.JwtResponseModel.JwtResponseModel;
import com.example.jwtbasic.JwtUserDetailsService.JwtUserDetailsService;
import com.example.jwtbasic.TokenManager.TokenManager;
import com.example.jwtbasic.model.DAOUser;
import com.example.jwtbasic.model.UserDTO;
//import com.spring.security.jwtbasic.jwtutils.models.JwtRequestModel;
//import com.spring.security.jwtbasic.jwtutils.models.JwtResponseModel;
//import com.example.jwtbasic.jwtentitymodel.UserDTO;

@RestController()
@CrossOrigin(origins="http://localhost:4200")
public class JwtController {
   @Autowired
   private JwtUserDetailsService userDetailsService;
   @Autowired
   private AuthenticationManager authenticationManager;
   @Autowired
   private TokenManager tokenManager;
   @PostMapping("/login")
   public ResponseEntity<?> createToken(@RequestBody JwtRequestModel
   request) throws Exception {
      try {
         authenticationManager.authenticate( new UsernamePasswordAuthenticationToken(request.getUsername(),
            request.getPassword())
         );
      } catch (DisabledException e) {
         throw new Exception("USER_DISABLED", e);
      } catch (BadCredentialsException e) {
         throw new Exception("INVALID_CREDENTIALS", e);
      }
      final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());
      final String jwtToken = tokenManager.generateJwtToken(userDetails);
      return ResponseEntity.ok(new JwtResponseModel(jwtToken));
   }
   @RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> saveUser(@RequestBody UserDTO user) throws Exception {
		return userDetailsService.savecheck(user);
	}
  
}