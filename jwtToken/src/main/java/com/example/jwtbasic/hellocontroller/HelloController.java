package com.example.jwtbasic.hellocontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.jwtbasic.JwtUserDetailsService.JwtUserDetailsService;
import com.example.jwtbasic.model.DAOUser; 


@RestController
@CrossOrigin
public class HelloController {
	  @Autowired
	   private JwtUserDetailsService userDetailsService;
	
	
   @GetMapping("/hello") 
   public String hello() { 
      return "hello"; 
   } 
   @RequestMapping(value = "/alldetails",method = RequestMethod.GET)
   public Iterable<DAOUser> getmethod(){
	   return userDetailsService.get();
	   
   }
}
