import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  username:any;
  password:any;
  invalidLogin = false

  constructor(private router: Router,
    private loginservice: AuthenticationService) { }

  ngOnInit() {

  }

  errormsg!:string;
  checkLogin() {
 if(this.username != "" &&this.username!=null && this.username!=undefined && this.password!=""){
 this.loginservice.authenticater(this.username, this.password).subscribe(data => {
      this.errormsg="";
      this.router.navigate(['login'])
      this.invalidLogin = false
      console.log(data); 
      
    },error => {
      console.log(error.error.text)
      this.invalidLogin = true;
      this.errormsg=error.error.text;
    }
  
    );
}
  }
}
