import { TestBed } from '@angular/core/testing';

import { BasichttpiterceptorService } from './basichttpiterceptor.service';

describe('BasichttpiterceptorService', () => {
  let service: BasichttpiterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BasichttpiterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
