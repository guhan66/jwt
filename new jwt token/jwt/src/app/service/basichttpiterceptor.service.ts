import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BasichttpiterceptorService implements HttpInterceptor{

  constructor(private http:HttpClient) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // throw new Error('Method not implemented.');

let token =sessionStorage.getItem('username')

if(!token){
  return next.handle(req);
}

let req1 =req.clone({
  headers:req.headers.set('Authorization',`Bearer ${token}`),
})
return next.handle(req1);
  }
}
