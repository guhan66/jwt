import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:any;
  password:any;
  invalidLogin = false

  constructor(private router: Router,
    private loginservice: AuthenticationService) { }

  ngOnInit() {

  }

  checkLogin() {
 this.loginservice.authenticate(this.username, this.password).subscribe(data => {
  
      this.router.navigate(['employee'])
      this.invalidLogin = false
      console.log(data);
    
    },error => {
      this.invalidLogin = true;
    }
    );
    
    
     
    // {
  //     this.router.navigate(['employee'])
  //     this.invalidLogin = false
  //   } else
  //     this.invalidLogin = true
  // }


}
}
